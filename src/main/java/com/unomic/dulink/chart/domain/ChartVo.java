
package com.unomic.dulink.chart.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;




@Getter
@Setter
@ToString
public class ChartVo{
	String workDate;
	String jig;
	String WC;
	String GRNM;
	String targetDateTime;
	int inCycleTime;
	int waitTime;
	int alarmTime;
	int noConnectionTime;
	String line;
	String sDate;
	String eDate;
	
	//common
	Integer shopId;
	String dvcId;
	String id;
	String pwd;
	String msg;
	String rgb;
	String name;
	
}
